﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameState : MonoBehaviour
{
    private static GameState _instance;

    public static GameState Instance { get { return _instance; } }

    private int scorePlayer = 0;


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate(){	
		GameObject.FindWithTag("scoreLabel").GetComponent<Text>().text = "" + scorePlayer;	
	}

    public void addScorePlayer(int toAdd){
        scorePlayer	+= toAdd;
    }	
    public int getScorePlayer(){
        return scorePlayer;
    }	
}
