﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveShip : MonoBehaviour{

    // Vitesse de déplacement
    public Vector2 speed;

    // Stockage du mouvement
    private Vector2 mouvement;

    // Update is called once per frame
    void Update()
    {
        // Récupérer les informations du clavier / manette

        float inputY = Input.GetAxis("Vertical");
        float inputX = Input.GetAxis("Horizontal");

        // Calcul du mouvement
        
        mouvement = new Vector2(
            speed.x * inputX,
            speed.y * inputY);
        

        GetComponent<Rigidbody2D> ().velocity = mouvement;

    }
}
