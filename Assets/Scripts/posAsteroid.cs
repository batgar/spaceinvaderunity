﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class posAsteroid : MonoBehaviour
{
    private GameObject asteroid;
    private Vector3 coinBasDroit;
    private Vector3 coinBasGauche;
    private Vector3 coinHautDroit;
    private Vector3 coinHautGauche;
    private Vector3 siz;
    
    private Vector3 tmpPos;
    // Start is called before the first frame update
    void Start()
    {
        tmpPos = new Vector3(coinBasDroit.x + (siz.x / 2),
                             Random.Range(coinHautDroit.y - siz.y / 3,coinBasDroit.y + siz.y / 3),
                             transform.position.z);


        coinBasGauche = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        coinBasDroit = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        coinHautGauche = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        coinHautDroit = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
    }

    // Update is called once per frame
    void Update()
    {
        siz.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
        siz.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;

        // Si la position en X de l'asteroid est inférieur à la limite gauche de l'écran, on repositionne l'astéroid à droite de l'écran + sa taille
        if (transform.position.x < coinBasGauche.x - (siz.x / 2) ){

            Destroy(gameObject);

        }
    }
}
