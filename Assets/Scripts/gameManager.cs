﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameManager : MonoBehaviour
{
    private Vector3 coinBasDroit;
    private Vector3 coinBasGauche;
    private Vector3 coinHautDroit;
    private Vector3 coinHautGauche;

    private int nombreMax = 10;
    private GameObject[] listeAsteroid;

    // Start is called before the first frame update
    void Start()
    {
        coinBasGauche = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        coinBasDroit = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        coinHautGauche = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        coinHautDroit = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));

        Debug.Log(coinHautDroit.y);
        Debug.Log(coinBasDroit.y);
    }

    // Update is called once per frame
    void Update()
    {

        listeAsteroid = GameObject.FindGameObjectsWithTag("asteroid");

        
        if (listeAsteroid.Length < nombreMax && Random.Range(0,20) == 5 ){

            float ry = Random.Range(1,100000);
            ry = ry / 100000 * 6;
            ry -= 3;


            Vector3 tmpPos = new Vector3(coinBasDroit.x,
                                    ry,
                                    0);

            GameObject gy = Instantiate(Resources.Load("asteroid"),tmpPos,Quaternion.identity) as GameObject;
        }
    }
}
