﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveAsteroid : MonoBehaviour
{

    private Vector2 speed = new Vector2( -5, 0 );
    private Vector2 mouvement;

    private int pv = 3;

    private float size;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        mouvement = new Vector2( speed.x, speed.y );

        GetComponent<Rigidbody2D> ().velocity = mouvement;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.name == "Ship"){
            if(GameObject.FindGameObjectsWithTag("life5").Length > 0){
                GameObject.FindGameObjectsWithTag("life5")[0].AddComponent<fadeOut>();
            }
            else if(GameObject.FindGameObjectsWithTag("life4").Length > 0){
                GameObject.FindGameObjectsWithTag("life4")[0].AddComponent<fadeOut>();
            }
            else if(GameObject.FindGameObjectsWithTag("life3").Length > 0){
                GameObject.FindGameObjectsWithTag("life3")[0].AddComponent<fadeOut>();
            }

            else if(GameObject.FindGameObjectsWithTag("life2").Length > 0){
                GameObject.FindGameObjectsWithTag("life2")[0].AddComponent<fadeOut>();
            }
            else{
                GameObject.FindGameObjectsWithTag("life1")[0].AddComponent<fadeOut>();
    
            }
        }
        if(other.tag == "ShootJaune"){
            pv -= 1;
            if(pv == 0){
                GameState.Instance.addScorePlayer(10);
                Destroy(gameObject);
            }
        }
    }
}
