﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveShoot : MonoBehaviour
{
    private Vector2 mouvement;
    private Vector3 coinBasDroit;


    // Start is called before the first frame update
    void Start()
    {
        coinBasDroit = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
    }

    // Update is called once per frame
    void Update()
    {
        mouvement = new Vector2(5, 0);
        
        GetComponent<Rigidbody2D> ().velocity = mouvement;

        if (transform.position.x > coinBasDroit.x){

            Destroy(gameObject);

        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "asteroid"){
                Destroy(gameObject);
        }
    }
}
