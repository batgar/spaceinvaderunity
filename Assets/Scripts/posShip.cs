﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class posShip : MonoBehaviour
{

    private Vector3 coinBasDroit;
    private Vector3 coinBasGauche;
    private Vector3 coinHautDroit;
    private Vector3 coinHautGauche;

    private Vector3 siz;
    
    // Start is called before the first frame update
    void Start()
    {
        coinBasGauche = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, 0));
        coinBasDroit = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, 0));
        coinHautGauche = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, 0));
        coinHautDroit = Camera.main.ViewportToWorldPoint(new Vector3(1, 1, 0));
    }

    // Update is called once per frame
    void Update()
    {
        siz.x = gameObject.GetComponent<SpriteRenderer> ().bounds.size.x;
        siz.y = gameObject.GetComponent<SpriteRenderer> ().bounds.size.y;

        // Si la position en Y du vaisseau est inférieur à la limite basse de l'écran, on repositionne le vaiseau en bas de l'écran
        if (transform.position.y < coinBasGauche.y + (siz.y / 2) ){
            gameObject.transform.position = new Vector3(transform.position.x,
                                                        coinBasGauche.y + (siz.y / 2),
                                                        transform.position.z);
        }

        // Si la position en Y du vaisseau est supérieur à la limite haute de l'écran, on repositionne le vaiseau en haut de l'écran
        if (transform.position.y > coinHautGauche.y - (siz.y / 2) ){
            gameObject.transform.position = new Vector3(transform.position.x,
                                                        coinHautGauche.y - (siz.y / 2),
                                                        transform.position.z);
        }

        // Si la position en X du vaisseau est inférieur à la limite gauche de l'écran, on repositionne le vaiseau à gauche de l'écran
        if (transform.position.x < coinBasGauche.x + (siz.x / 3) ){
            gameObject.transform.position = new Vector3(coinBasGauche.x + (siz.x / 3),
                                                        transform.position.y,
                                                        transform.position.z);
        }

        // Si la position en X du vaisseau est supérieur à la limite droite de l'écran, on repositionne le vaiseau à droite de l'écran
        if (transform.position.x > coinBasDroit.x - (siz.x / 3) ){
            gameObject.transform.position = new Vector3(coinBasDroit.x - (siz.x / 3),
                                                        transform.position.y,
                                                        transform.position.z);
        }
    }
}
